package pucp.edu.cohmetrixesp.utils;

import edu.upc.Jfreeling.ChartParser;
import edu.upc.Jfreeling.HmmTagger;
import edu.upc.Jfreeling.Maco;
import edu.upc.Jfreeling.Nec;
import edu.upc.Jfreeling.Senses;
import edu.upc.Jfreeling.Splitter;
import edu.upc.Jfreeling.Tokenizer;
import edu.upc.Jfreeling.Ukb;

public interface IFreelingAnalyzer {
	Maco getMorfological();
	Tokenizer getTokenizer();
	Splitter getSplitter();
	HmmTagger getPosTagger();
	ChartParser getChunkParser();
	Nec getNec();
	Senses getSenses();
	Ukb getDisambiguator();
}
