package pucp.edu.cohmetrixesp.structs;

import java.util.Iterator;

import edu.upc.Jfreeling.ListWordIterator;
import edu.upc.Jfreeling.Sentence;
import edu.upc.Jfreeling.Word;

public class FreelingWordIterable implements Iterable<Word> {
	ListWordIterator lword;

	public FreelingWordIterable(Sentence sentence) {
		lword = new ListWordIterator(sentence);
	}

	@Override
	public Iterator<Word> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<Word>() {
			@Override
			public boolean hasNext() {
				return lword.hasNext();
			}

			@Override
			public Word next() {
				return lword.next();
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}

}
